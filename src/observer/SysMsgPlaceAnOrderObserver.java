package observer;

/**
 * 发送系统消息
 *
 * @author lixin
 */
public class SysMsgPlaceAnOrderObserver extends AbstractPlaceAnOrderObserver {

    SysMsgPlaceAnOrderObserver(PlaceAnOrderSubject subject) {
        this.subject = subject;
        this.subject.register(this);
    }

    @Override
    public void success() {
        System.out.println("发送系统消息：你的订单下单成功");
    }

    @Override
    public void fail() {
        System.out.println("发送系统消息：你的订单下单失败");
    }
}
