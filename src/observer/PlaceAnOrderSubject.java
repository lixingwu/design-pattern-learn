package observer;

import java.util.ArrayList;
import java.util.List;

/**
 * 下单操作，下单主题
 *
 * @author lixin
 */
public class PlaceAnOrderSubject {
    private final List<AbstractPlaceAnOrderObserver> observers = new ArrayList<>();

    /**
     * 订单状态控制
     */
    private int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    /**
     * 注册
     *
     * @param observer 下单后处理类
     */
    public void register(AbstractPlaceAnOrderObserver observer) {
        observers.add(observer);
    }

    /**
     * 注销
     *
     * @param observer 下单后处理类
     */
    public void cancellation(AbstractPlaceAnOrderObserver observer) {
        observers.remove(observer);
    }

    /**
     * 下单成功通知已经注册的对象
     */
    public void notifySuccess() {
        for (AbstractPlaceAnOrderObserver observer : observers) {
            observer.success();
        }
    }

    /**
     * 下单失败通知已经注册的对象
     */
    public void notifyFail() {
        for (AbstractPlaceAnOrderObserver observer : observers) {
            observer.fail();
        }
    }
}
