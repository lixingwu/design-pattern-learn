package observer;

/**
 * 下单后处理抽象类
 *
 * @author lixin
 */
public abstract class AbstractPlaceAnOrderObserver {
    protected PlaceAnOrderSubject subject;

    /**
     * 成功通知
     */
    public abstract void success();

    /**
     * 失败通知
     */
    public abstract void fail();
}
