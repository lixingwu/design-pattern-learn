package observer;

/**
 * 购物车处理
 *
 * @author lixin
 */
public class ShopCartPlaceAnOrderObserver extends AbstractPlaceAnOrderObserver {

    ShopCartPlaceAnOrderObserver(PlaceAnOrderSubject subject) {
        this.subject = subject;
        this.subject.register(this);
    }

    @Override
    public void success() {
        System.out.println("购物车处理：你的订单下单成功");
    }

    @Override
    public void fail() {
        System.out.println("购物车处理：你的订单下单失败");
    }
}
