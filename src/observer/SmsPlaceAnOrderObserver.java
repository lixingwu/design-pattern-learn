package observer;

/**
 * 发送短信处理
 *
 * @author lixin
 */
public class SmsPlaceAnOrderObserver extends AbstractPlaceAnOrderObserver {

    SmsPlaceAnOrderObserver(PlaceAnOrderSubject subject) {
        this.subject = subject;
        this.subject.register(this);
    }

    @Override
    public void success() {
        System.out.println("短信发送：你的订单下单成功");
    }

    @Override
    public void fail() {
        System.out.println("短信发送：你的订单下单失败");
    }
}
