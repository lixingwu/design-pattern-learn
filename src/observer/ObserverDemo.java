package observer;


/**
 * 观察者模式：
 * 定义对象间的一种一对多的依赖关系，当一个对象的状态发生改变时，所有依赖于它的对象都得到通知并被自动更新。
 *
 * @author lixin
 */
public class ObserverDemo {
    public static void main(String[] args) {

        // 创建主题
        PlaceAnOrderSubject subject = new PlaceAnOrderSubject();

        // 传入主题对象，让观察者注册到主题对象中
        new SmsPlaceAnOrderObserver(subject);
        new SysMsgPlaceAnOrderObserver(subject);
        new ShopCartPlaceAnOrderObserver(subject);

        // 业务处理成功，通知注册过的对象
        subject.notifySuccess();

        // 业务处理失败，通知注册过的对象
        //subject.notifyFail();

    }
}
