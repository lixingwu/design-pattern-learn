package singleton;

public class Test {
    public static void main(String[] args) {
        PrintDevice device1 = PrintDevice.getInstance();
        System.out.println(device1.open());
        System.out.println(device1.print());
        System.out.println(device1.close());

        PrintDevice device2 = PrintDevice.getInstance();
        System.out.println(device2.print());
        System.out.println(device1.close());
        System.out.println(device1.open());
        System.out.println(device2.print());

        SingFactory factory = SingFactory.getInstance();
        SingFactory factory2 = SingFactory.getInstance();
        System.out.println(factory);
        System.out.println(factory2 );
    }
}
