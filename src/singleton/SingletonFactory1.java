package singleton;

/**
 * 单例模式01
 *
 * @author lixin
 */
public class SingletonFactory1 {

    private static SingletonFactory1 instance;

    // 私有构造器，禁止其他类使用 new 调用
    private SingletonFactory1() {
    }

    // 提供一个全局的访问点
    public static SingletonFactory1 getInstance() {
        if (null == instance) {
            instance = new SingletonFactory1();
        }
        return instance;
    }

    // SingletonFactory1 的方法
    public void showMsg() {
        System.out.println("message");
    }

}
