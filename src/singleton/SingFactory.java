package singleton;

import java.io.ObjectStreamException;
import java.io.Serializable;

/**
 * 双重检查加锁法
 */
public class SingFactory implements Serializable {
    // volatile关键字
    // 1）保证了不同线程对这个变量进行操作时的可见性，即一个线程修改了某个变量的值，这新值对其他线程来说是立即可见的。
    // 2）禁止进行指令重排序
    public volatile static SingFactory singleton = new SingFactory();

    // 私有化构造函数，禁止外部进行创建对象
    private SingFactory() {
        // 在构造器中加个逻辑判断,多次调用抛出异常
        // 防止使用 反射 获取构造方法
        if (singleton != null) {
            throw new RuntimeException();
        }
    }

    // 使用静态方法进行获取对象
    // 如果对象不存在，则进行加锁创建对象
    public static SingFactory getInstance() {
        if (singleton == null) {
            synchronized (SingFactory.class) {
                if (singleton == null) {
                    singleton = new SingFactory();
                }
            }
        }
        return singleton;
    }

    // 反序列化定义该方法，则不需要创建新对象
    private Object readResolve() throws ObjectStreamException {
        return singleton;
    }

    public void print() {
        System.out.println("printprint");
    }


}
