package singleton;

public class PrintDevice {
    private boolean isOpen = false;//是否开机
    private boolean isPrint = false;//是否处于打印中
    public volatile static PrintDevice singPrint = new PrintDevice();

    private PrintDevice() {
        this.isOpen = false;
        this.isPrint = false;
        System.out.println("初始化中...");
    }

    public static PrintDevice getInstance() {
        if (singPrint == null) {
            synchronized (SingFactory.class) {
                if (singPrint == null) {
                    singPrint = new PrintDevice();
                }
            }
        }
        return singPrint;
    }

    public boolean open() {
        if (!isOpen) {
            this.isOpen = true;
            this.isPrint = false;
            System.out.print("打开中...");
        }
        return true;
    }

    public boolean close() {
        if (isOpen) {
            this.isOpen = false;
            this.isPrint = false;
            System.out.print("关闭中...");
        }
        return true;
    }

    public boolean print() {
        if (isOpen && !isPrint) {
            this.isPrint = true;
            System.out.print("打印中...");
            return true;
        }
        return false;
    }

}
