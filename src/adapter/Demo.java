package adapter;

/**
 * 测试能否开机
 *
 * @author lixin
 */
public class Demo {

    public static void main(String[] args) throws InterruptedException {
        // 创建一台电脑
        IComputer computer = new Computer();

        // 15V的电压是标准的电压，可以直接给电脑使用
        // computer.inPower(new Power15V());

        // 电脑没法直接使用220V电源
        // computer.inPower(new Power220V());
        // 这时我们可以在中间插一个电源适配，把220V电压转化为电脑的标准电压15V
        IPower power = new PowerAdapter(new Power220V());
        computer.inPower(power);

        // 初始化电脑
        computer.init();

        // 查看状态，电压异常会抛出异常
        System.out.println(computer.status());
    }

}
