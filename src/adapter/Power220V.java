package adapter;

/**
 * 电源实现类
 *
 * @author lixin
 */
public class Power220V implements IPower2 {

    /**
     * 直插电压
     *
     * @return 输出220V电压
     */
    @Override
    public long outPower() {
        return 220L;
    }
}
