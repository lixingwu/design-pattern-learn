package adapter;

/**
 * 电脑电源适配器
 *
 * @author lixin
 */
public class PowerAdapter implements IPower {

    /*** 需要适配的电源 */
    private final IPower2 power;

    public PowerAdapter(IPower2 power) {
        this.power = power;
    }

    @Override
    public int outPower() {
        int normalPower;
        System.out.println("计算机额定电压为：" + IComputer.RATED_V + "V");
        System.out.println("电压输入电压为：" + this.power.outPower() + "V");
        System.out.println("电压转换中...");
        normalPower = this.conversionVoltage(power.outPower());
        System.out.println("电压转换完成...");
        return normalPower;
    }

    /**
     * 把输入电压转化为额定电压
     *
     * @param in 输入电压
     */
    private int conversionVoltage(long in) {
        // 输入电压和额定电压一致的，不做如何操作
        if (in == IComputer.RATED_V) {
            System.out.println("电压正常，直接输出。");
        } else if (in < IComputer.RATED_V) {
            System.out.println("电压过低，电压：+" + (IComputer.RATED_V - in) + "V");
        } else {
            System.out.println("电压过高，电压：-" + (in - IComputer.RATED_V) + "V");
        }
        return IComputer.RATED_V;
    }
}
