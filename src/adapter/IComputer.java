package adapter;

/**
 * 电脑接口
 *
 * @author lixin
 */
public interface IComputer {

    /*** 额定电压 */
    Integer RATED_V = 15;

    /**
     * 输入电源
     *
     * @param power 电源
     * @throws IllegalArgumentException 电流电压参数异常
     * @author "miaopasi"
     */
    void inPower(IPower power) throws IllegalArgumentException;

    /**
     * 电脑状态
     *
     * @return false 未通电，true通电
     */
    boolean status();

    /**
     * 初始化电脑
     *
     * @throws InterruptedException 初始化异常
     * @author "miaopasi"
     */
    void init() throws InterruptedException;

}
