package adapter;

/**
 * 新的电源接口
 *
 * @author lixin
 */
public interface IPower2 {

    /**
     * 输出电源
     *
     * @return 返回电压
     * @author "miaopasi"
     */
    long outPower();

}
