package adapter;

/**
 * 电脑现实类
 *
 * @author lixin
 */
public class Computer implements IComputer {

    /*** 通电状态 */
    private Boolean status = Boolean.FALSE;

    @Override
    public void inPower(IPower power) throws IllegalArgumentException {
        if (power.outPower() > RATED_V) {
            throw new IllegalArgumentException("电源输入电压过高");
        }
        if (power.outPower() < RATED_V) {
            throw new IllegalArgumentException("电源输入电压过低");
        }
        this.status = true;
    }

    @Override
    public boolean status() {
        return this.status;
    }

    @Override
    public void init() throws InterruptedException {
        System.out.println("初始化电脑中...");
        Thread.sleep(2000);
        System.out.println("初始化电脑完成...");
    }
}
