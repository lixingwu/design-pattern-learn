package adapter;

/**
 * 电源接口
 *
 * @author lixin
 */
public interface IPower {

    /**
     * 输出电源
     *
     * @return 返回电压
     * @author "miaopasi"
     */
    int outPower();

}
