package adapter;

/**
 * 电源实现类
 *
 * @author lixin
 */
public class Power implements IPower {

    /**
     * 直插电压
     *
     * @return 输出15V电压
     */
    @Override
    public int outPower() {
        return 15;
    }
}
