package strategy;

/**
 * 还款环境类，用于自动切换还款算法
 *
 * @author lixin
 */
public class RepaymentContext {

    /**
     * 还款策略
     */
    private final RepaymentStrategy repaymentStrategy;

    public RepaymentContext(RepaymentStrategy repaymentStrategy) {
        this.repaymentStrategy = repaymentStrategy;
    }

    /**
     * 计算当前起数需要还款的金额，单位分便于计算
     *
     * @param totalAmount  借款的总金额
     * @param phase        借款的期数
     * @param interestRate 基准利率
     * @param currentPhase 还款的期数
     */
    public void repayment(long totalAmount, int phase, double interestRate, int currentPhase) {
        // 计算还款金额
        Long repaymentAmount = repaymentStrategy.repayment(totalAmount, phase, interestRate, currentPhase);
        // 在这里做些其他的事情
        System.out.println("还款的金额=" + repaymentAmount + "分");
    }

}
