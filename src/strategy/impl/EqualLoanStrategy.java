package strategy.impl;

import strategy.RepaymentStrategy;

/**
 * 等额本息法：每月的还款额相同
 * 每月还款额=〔贷款本金×月利率×(1＋月利率)＾还款月数〕÷〔(1＋月利率)＾还款月数-1〕
 *
 * @author lixin
 */
public class EqualLoanStrategy implements RepaymentStrategy {

    @Override
    public Long repayment(long totalAmount, int phase, double interestRate, int currentPhase) {
        System.out.println("等额本息法");
        return 0L;
    }

}
