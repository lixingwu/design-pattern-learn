package strategy.impl;

import strategy.RepaymentStrategy;

/**
 * 等额本金法：将贷款本金按还款的总月数均分，再加上上期剩余本金的利息
 * 每月还本付息金额=(本金/还款月数)+[(本金-累计已还本金)×月利率]
 *
 * @author lixin
 */
public class EqualPrincipalStrategy implements RepaymentStrategy {

    @Override
    public Long repayment(long totalAmount, int phase, double interestRate, int currentPhase) {
        System.out.println("等额本金法");
        return null;
    }

}
