package strategy;

import strategy.impl.EqualLoanStrategy;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 测试
 *
 * @author lixin
 */
public class ClientDemo {
    public static void main(String[] args) {
        RepaymentContext context = new RepaymentContext(new EqualLoanStrategy());

        // 计算月利率
        double interestRate = BigDecimal.valueOf(4.9d)
                .divide(BigDecimal.valueOf(100L), 5, RoundingMode.CEILING)
                .divide(BigDecimal.valueOf(12L), 5, RoundingMode.CEILING)
                .doubleValue();

        // 还款操作
        context.repayment(100 * 10000 * 100, 12 * 30, interestRate, 1);
    }
}
