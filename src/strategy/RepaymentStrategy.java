package strategy;

/**
 * 还款策略接口
 *
 * @author lixin
 */
public interface RepaymentStrategy {
    /**
     * 计算当前起数需要还款的金额，单位分便于计算
     *
     * @param totalAmount  借款的总金额
     * @param phase        借款的期数
     * @param interestRate 基准利率
     * @param currentPhase 还款的期数
     * @return 还款的金额
     */
    Long repayment(long totalAmount, int phase, double interestRate, int currentPhase);
}
