package handler;

/**
 * 老板处理类
 *
 * @author lixin
 */
public class BossHandler extends Handler {

    @Override
    public void approve(int day) {
        if (day > 3) {
            System.out.println("请假" + day + "天：老板审批通过");
        }
        if (handler != null) {
            handler.approve(day);
        }
    }

}
