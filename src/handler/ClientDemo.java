package handler;

/**
 * 测试责任链
 *
 * @author lixin
 */
public class ClientDemo {

    public static void main(String[] args) {

        Handler leader = new LeaderHandler();
        Handler boss = new BossHandler();

        // 设置链的顺序
        leader.setNext(boss);

        // day = 2，请求数据经过链
        // 输出：请假2天：部门经理审批通过
        leader.approve(2);

        // day = 5，请求数据经过链
        // 输出：
        // 请假5天：部门经理审批通过
        // 请假5天：老板审批通过
        leader.approve(5);

    }


}
