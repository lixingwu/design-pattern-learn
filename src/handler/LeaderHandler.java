package handler;

/**
 * 部门经理处理
 *
 * @author lixin
 */
public class LeaderHandler extends Handler {

    @Override
    public void approve(int day) {
        if (day > 0) {
            System.out.println("请假" + day + "天：部门经理审批通过");
        }
        if (handler != null) {
            handler.approve(day);
        }
    }
}
