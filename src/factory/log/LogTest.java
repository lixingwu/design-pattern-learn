package factory.log;


import factory.log.database.DatabaseLogFactory;
import factory.log.local.LocalFileLogFactory;
import factory.log.service.ServiceLogFactory;

public class LogTest {
    public static void main(String[] args) {

        System.out.println("// 输出：");

        IAbstractMyLogFactory serviceLogFactory = new ServiceLogFactory();
        AbstractMyLog serviceLog = serviceLogFactory.createMyLog();
        serviceLog.info("哈哈哈");

        IAbstractMyLogFactory databaseLogFactory = new DatabaseLogFactory();
        AbstractMyLog databaseLog = databaseLogFactory.createMyLog();
        databaseLog.info("哈哈哈");

        IAbstractMyLogFactory localFileLogFactory = new LocalFileLogFactory();
        AbstractMyLog localLog = localFileLogFactory.createMyLog();
        localLog.info("哈哈哈");
    }
}
