package factory.log;

public interface IAbstractMyLogFactory {
    /**
     * 创建对应
     *
     * @return 具体实现类
     */
    AbstractMyLog createMyLog();
}
