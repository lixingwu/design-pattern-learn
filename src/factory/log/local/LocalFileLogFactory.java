package factory.log.local;


import factory.log.AbstractMyLog;
import factory.log.IAbstractMyLogFactory;

/**
 * 本地文件实现类创建工厂
 */
public class LocalFileLogFactory implements IAbstractMyLogFactory {

    @Override
    public AbstractMyLog createMyLog() {
        return new LocalFileLog();
    }

}
