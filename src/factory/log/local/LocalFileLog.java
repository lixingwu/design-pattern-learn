package factory.log.local;

import factory.log.AbstractMyLog;

/**
 * 本地文件实现类
 */
public class LocalFileLog extends AbstractMyLog {
    @Override
    public void info(String msg) {
        System.out.println("// 本地文件 <<< [" + msg + "]");
    }
}
