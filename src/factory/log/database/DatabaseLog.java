package factory.log.database;

import factory.log.AbstractMyLog;

/**
 * 数据库实现类
 */
public class DatabaseLog extends AbstractMyLog {
    @Override
    public void info(String msg) {
        System.out.println("// 数据库 <<< [" + msg + "]");
    }
}
