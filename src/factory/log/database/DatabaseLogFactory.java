package factory.log.database;


import factory.log.AbstractMyLog;
import factory.log.IAbstractMyLogFactory;

/**
 * 数据库创建工厂
 */
public class DatabaseLogFactory implements IAbstractMyLogFactory {

    @Override
    public AbstractMyLog createMyLog() {
        return new DatabaseLog();
    }

}
