package factory.log;

public abstract class AbstractMyLog {

    /**
     * 记录日志
     *
     * @param msg 日志内容
     */
    public abstract void info(String msg);
}
