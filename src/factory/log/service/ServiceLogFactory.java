package factory.log.service;


import factory.log.AbstractMyLog;
import factory.log.IAbstractMyLogFactory;

/**
 * 保存远程服务器实现类创建工厂
 */
public class ServiceLogFactory implements IAbstractMyLogFactory {

    @Override
    public AbstractMyLog createMyLog() {
        return new ServiceLog();
    }

}
