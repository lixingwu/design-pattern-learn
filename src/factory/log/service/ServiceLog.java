package factory.log.service;
import factory.log.AbstractMyLog;

/**
 * 保存远程服务器实现类
 */
public class ServiceLog extends AbstractMyLog {
    @Override
    public void info(String msg) {
        System.out.println("// 远程服务器 <<< [" + msg + "]");
    }
}
