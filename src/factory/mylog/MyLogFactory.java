package factory.mylog;

import factory.mylog.impl.DatabaseLog;
import factory.mylog.impl.LocalFileLog;
import factory.mylog.impl.ServiceLog;

/**
 * 日志工厂类
 *
 * @author https://www.cnblogs.com/lixingwu/
 */
public class MyLogFactory {

    /**
     * 根据类型返回不同的实现类
     *
     * @param type 类型
     * @return 实现类的抽象
     */
    public static AbstractMyLog get(int type) {
        AbstractMyLog log = new LocalFileLog();
        switch (type) {
            case 1: log = new DatabaseLog();break;
            case 2: log = new ServiceLog();break;
            default: break;
        }
        return log;
    }
}
