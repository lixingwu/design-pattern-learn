package factory.mylog.impl;

import factory.mylog.AbstractMyLog;

/**
 * 保存到本地文件实现类
 *
 * @author https://www.cnblogs.com/lixingwu/
 */
public class LocalFileLog extends AbstractMyLog {
    @Override
    public void info(String msg) {
        System.out.println("// 本地文件 <<< [" + msg + "]");
    }
}
