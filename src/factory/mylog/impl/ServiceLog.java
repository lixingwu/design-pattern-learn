package factory.mylog.impl;

import factory.mylog.AbstractMyLog;

/**
 * 保存远程服务器实现类
 *
 * @author https://www.cnblogs.com/lixingwu/
 */
public class ServiceLog extends AbstractMyLog {
    @Override
    public void info(String msg) {
        System.out.println("// 远程服务器 <<< [" + msg + "]");
    }
}
