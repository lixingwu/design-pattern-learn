package factory.mylog.impl;

import factory.mylog.AbstractMyLog;

/**
 * 保存到数据库实现类
 *
 * @author https://www.cnblogs.com/lixingwu/
 */
public class DatabaseLog extends AbstractMyLog {
    @Override
    public void info(String msg) {
        System.out.println("// 数据库 <<< [" + msg + "]");
    }
}
