package factory.mylog;

/**
 * 日志记录器 抽象类
 *
 * @author https://www.cnblogs.com/lixingwu/
 */
public abstract class AbstractMyLog {

    /**
     * 记录日志
     *
     * @param msg 日志内容
     */
    public abstract void info(String msg);
}
