package factory.mylog;

/**
 * 测试
 *
 * @author https://www.cnblogs.com/lixingwu/
 */
public class MyLogTest {

    public static void main(String[] args) {

        System.out.println("// 输出结果：");

        AbstractMyLog log = MyLogFactory.get(0);
        log.info("你好鸭");

        AbstractMyLog log1 = MyLogFactory.get(1);
        log1.info("你好鸭1");

        AbstractMyLog log2 = MyLogFactory.get(2);
        log2.info("你好鸭2");
    }

}
