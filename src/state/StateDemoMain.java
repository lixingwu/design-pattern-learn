package state;

import state.impl.OrderExpireState;
import state.impl.PayCompleteState;
import state.impl.PayIngState;

/**
 * 状态模式测试类
 *
 * @author lixin
 */
public class StateDemoMain {

    public static void main(String[] args) {
        // 创建Context使用状态
        OrderDetailContext context = new OrderDetailContext();

        // 改变状态
        context.setOrderState(new PayIngState());
        // 执行对应状态的处理方法
        OrderDetail detail1 = context.findOrderDetail(1L);
        System.out.println(detail1);

        System.out.println("==================");
        // 切换到另外一个状态
        context.setOrderState(new PayCompleteState());
        // 执行对应状态的处理方法
        OrderDetail detail2 = context.findOrderDetail(2L);
        System.out.println(detail2);

        System.out.println("==================");
        // 切换到另外一个状态
        context.setOrderState(new OrderExpireState());
        // 执行对应状态的处理方法
        OrderDetail detail3 = context.findOrderDetail(3L);
        System.out.println(detail3);
    }

}
