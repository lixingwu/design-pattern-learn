package state;

/**
 * 订单状态抽象
 *
 * @author lixin
 */
public interface OrderState {

    /**
     * 查询订单的详细信息
     *
     * @param context 订单查询类
     * @param orderId 订单ID
     * @return 订单详情实体
     */
    OrderDetail findOrderDetail(OrderDetailContext context, Long orderId);

}
