package state;

/**
 * 订单操作类
 *
 * @author lixin
 */
public class OrderDetailContext {
    private OrderState orderState;

    public OrderDetailContext() {
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    /**
     * 查询订单详情
     *
     * @param orderId 订单ID
     * @return 订单详情
     */
    public OrderDetail findOrderDetail(Long orderId) {
        return orderState.findOrderDetail(this, orderId);
    }
}
