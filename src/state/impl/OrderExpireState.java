package state.impl;

import state.OrderDetail;
import state.OrderDetailContext;
import state.OrderState;

/**
 * 订单过期
 *
 * @author lixin
 */
public class OrderExpireState implements OrderState {

    @Override
    public OrderDetail findOrderDetail(OrderDetailContext context, Long orderId) {
        context.setOrderState(this);
        System.out.println("订单过期...");
        return findBiId(orderId);
    }

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderId 订单ID
     * @return 订单信息
     */
    private OrderDetail findBiId(Long orderId) {
        System.out.println(String.format("根据orderId=%d到过期订单表中查询信息...", orderId));
        return new OrderDetail(3L, "套餐99费用", "ZM2021021016560345", -1);
    }
}
