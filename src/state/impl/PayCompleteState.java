package state.impl;

import state.OrderDetail;
import state.OrderDetailContext;
import state.OrderState;

/**
 * 订单支付完成
 *
 * @author lixin
 */
public class PayCompleteState implements OrderState {

    @Override
    public OrderDetail findOrderDetail(OrderDetailContext context, Long orderId) {
        context.setOrderState(this);
        System.out.println("订单支付完成...");
        return findBiId(orderId);
    }

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderId 订单ID
     * @return 订单信息
     */
    private OrderDetail findBiId(Long orderId) {
        System.out.println(String.format("根据orderId=%d到完成的订单表中查询记录的信息...", orderId));
        return new OrderDetail(2L, "套餐3费用", "ZM2021021016562514", 1);
    }
}
