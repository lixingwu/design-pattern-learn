package state.impl;

import state.OrderDetail;
import state.OrderDetailContext;
import state.OrderState;

/**
 * 订单支付中
 *
 * @author lixin
 */
public class PayIngState implements OrderState {

    @Override
    public OrderDetail findOrderDetail(OrderDetailContext context, Long orderId) {
        context.setOrderState(this);
        System.out.println("订单支付中...");
        OrderDetail orderDetail = findBiId(orderId);
        updateOrderState(orderDetail.getOrderNo());
        return findBiId(orderId);
    }

    /**
     * 到微信上查询订单是否交易完成
     * 如果完成就更新一下订单得到的状态
     *
     * @param orderNo 订单号
     */
    private void updateOrderState(String orderNo) {
        System.out.println("根据订单号到微信支付同步订单状态...");
    }

    /**
     * 根据订单ID查询订单信息
     *
     * @param orderId 订单ID
     * @return 订单信息
     */
    private OrderDetail findBiId(Long orderId) {
        System.out.println(String.format("根据orderId=%d查询到订单信息...", orderId));
        return new OrderDetail(1L, "手续费", "ZM2021021016560458", 0);
    }

}
