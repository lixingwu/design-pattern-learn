package state;

/**
 * 订单详情实体
 *
 * @author lixin
 */
public class OrderDetail {
    private Long id;
    private String name;
    private String orderNo;
    private Integer status;

    public OrderDetail(Long id, String name, String orderNo, Integer status) {
        this.id = id;
        this.name = name;
        this.orderNo = orderNo;
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", orderNo='" + orderNo + '\'' +
                ", status=" + status +
                '}';
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

}
