package proxy.virtual;

import java.util.LinkedList;
import java.util.List;

/**
 * 老板实现这个接口主要是处理提交的任务
 *
 * @author lixin
 */
public class Boss implements Approvable {

    List<String> orders;

    public Boss(List<String> orders) {
        System.out.println("神秘Boss出现了...");
        this.orders = orders;
    }

    /**
     * 添加任务到待处理列表
     *
     * @param order 任务
     */
    @Override
    public void addOrder(String order) {
        if (this.orders == null) {
            this.orders = new LinkedList<>();
        }
        this.orders.add(order);
    }

    @Override
    public void approve() {
        while (this.orders.size() > 0) {
            String order = orders.remove(0);
            System.out.println("神秘Boss查看了：" + order);
        }
    }

}
