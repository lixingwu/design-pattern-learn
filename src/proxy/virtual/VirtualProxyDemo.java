package proxy.virtual;

/**
 * 测试虚拟代理
 *
 * @author lixin
 */
public class VirtualProxyDemo {
    public static void main(String[] args) {
        // 创建老板的代理
        Approvable secretary = new BossProxy();

        // 收集任务
        secretary.addOrder("合同签名");
        secretary.addOrder("明天开会");

        System.out.println("2小时后...");

        // 处理任务，此时才创建老板对象
        secretary.approve();

        // 老板直接收任务
        secretary.addOrder("约饭");
        secretary.addOrder("打球");

        // 处理任务
        secretary.approve();

    }
}
