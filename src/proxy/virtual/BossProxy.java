package proxy.virtual;

import java.util.LinkedList;
import java.util.List;

/**
 * 老板的代理（秘书）实现这个接口主要是收集任务，然后提交给老板，收集任务时不用神秘老板出现，
 * 如果老板出现了，那老板应该直自己查看这些任务，不用秘书再告诉老板
 *
 * @author lixin
 */
public class BossProxy implements Approvable {
    List<String> orders;
    volatile Boss boss;

    /**
     * 秘书把收集的任务添加到待处理列表
     *
     * @param order 任务
     */
    @Override
    public void addOrder(String order) {
        // 如果boss已经出现，由boss自己添加到列表
        if (this.boss != null) {
            System.out.println("老板亲自添加任务：" + order);
            this.boss.addOrder(order);
        } else {
            // 如果boss不在，由秘书代理boss添加到列表
            if (this.orders == null) {
                this.orders = new LinkedList<>();
            }
            System.out.println("老板不在，秘书帮老板添加任务：" + order);
            this.orders.add(order);
        }
    }

    @Override
    public void approve() {
        // 提交给老板处理，如果老板不在就找到老板，把收集的任务提交给老板
        if (this.boss == null) {
            synchronized (this) {
                if (this.boss == null) {
                    this.boss = new Boss(this.orders);
                }
            }
        }
        boss.approve();
    }

}
