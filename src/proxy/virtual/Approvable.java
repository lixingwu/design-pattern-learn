package proxy.virtual;

/**
 * 处理类，处理收集的任务。
 * 老板的代理（秘书）实现这个接口主要是收集任务，然后提交给老板
 * 老板实现这个接口主要是处理提交的任务
 *
 * @author lixin
 */
public interface Approvable {

    /**
     * 处理任务
     */
    void approve();

    /**
     * 添加任务
     *
     * @param order 任务
     */
    void addOrder(String order);

}
