package facade;

/**
 * 生成封面业务类
 *
 * @author lixin
 */
public class CreateCoverService {

    /**
     * 提取指定视频文件的封面
     *
     * @param path 视频文件的路径
     * @return 返回封面图片的路径
     */
    public String cover(String path) {
        System.out.println("视频【" + path + "】封面提取成功！");
        return "/www/file/cover.jpg";
    }

}
