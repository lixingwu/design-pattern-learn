package facade;

/**
 * 保存文件业务类
 *
 * @author lixin
 */
public class SaveFileService {

    /**
     * 保存文件
     *
     * @param filename 文件名
     * @return 文件在服务器删的地址
     */
    public String save(String filename) {
        System.out.println(filename + " 文件保存成功！");
        return "/www/file/" + filename;
    }

}
