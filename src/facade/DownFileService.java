package facade;

/**
 * 下载文件处理类
 *
 * @author lixin
 */
public class DownFileService {

    /**
     * 下载指定文件
     *
     * @param path 文件的路径
     */
    public void down(String path) {
        System.out.println("文件【" + path + "】已保存到【D:\\down】目录下");
    }
}
