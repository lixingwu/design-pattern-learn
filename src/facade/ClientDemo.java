package facade;

/**
 * 客户端测试程序
 *
 * @author lixin
 */
public class ClientDemo {

    public static void main(String[] args) {
        ClientDemo client = new ClientDemo();
        //client.oldApi();
        client.newApi();
    }

    /**
     * 旧的接口
     */
    public void oldApi() {
        // 1.保存文件
        SaveFileService saveFileService = new SaveFileService();
        String filePath = saveFileService.save("东京不太热.avi");
        // 2.生成封面
        CreateCoverService coverService = new CreateCoverService();
        String coverPath = coverService.cover(filePath);
        // 3.下载封面
        DownFileService downFile = new DownFileService();
        downFile.down(coverPath);
    }

    /**
     * 新的接口
     */
    public void newApi() {
        CoverFacade facade = new CoverFacade();
        facade.cover("东京不太热.avi");
    }
}
