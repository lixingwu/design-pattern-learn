package facade;

/**
 * 封面外观类，处理视频封面的类
 *
 * @author lixin
 */
public class CoverFacade {
    SaveFileService saveFileService = new SaveFileService();
    CreateCoverService coverService = new CreateCoverService();
    DownFileService downFile = new DownFileService();

    /**
     * 生成视频的封面地址
     *
     * @param filename 文件名
     */
    public void cover(String filename) {
        // 1.保存文件
        String filePath = saveFileService.save(filename);
        // 2.生成封面
        String coverPath = coverService.cover(filePath);
        // 3.下载封面
        downFile.down(coverPath);
    }

}
