### 设计模式学习代码
文章看博客 [喵喵扑](https://www.cnblogs.com/lixingwu/category/1028508.html)
---
学习设计模式参考：
* [深入设计模式](https://refactoringguru.cn/design-patterns/catalog)
* [图说设计模式](https://design-patterns.readthedocs.io/zh_CN/latest/index.html)
* [设计模式（菜鸟教程）](https://www.runoob.com/design-pattern/design-pattern-tutorial.html)
* [Head First 设计模式](https://github.com/RongleXie/java-books-collections/blob/master/Head%20First%20%E8%AE%BE%E8%AE%A1%E6%A8%A1%E5%BC%8F(%E4%B8%AD%E6%96%87%E7%89%88).pdf)
